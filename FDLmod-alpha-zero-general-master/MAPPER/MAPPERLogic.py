'''

'''

class verb():
    def __init__(self): self.verbose = True
    def vprint(self, *args):
        if self.verbose == True: print(*args)
c = verb()
c.verbose = False

import numpy as np
import random
import copy
import itertools

DEFAULT_HEIGHT = 6
DEFAULT_WIDTH = 7
DEFAULT_TERMINAL_TIMESTEPS = 20


class Agent():
    def __init__(self, name=None, pos = None, battery = None):
        self.name = name
        self.pos = pos # position [x,y]
        if pos is None:
            self.pos = [0,0]
            
        self.bitmap = None
    
        self.battery = battery
        if battery is None: self.battery = 100. # battery starts at 100
        
        self.battery_cap = 100.
        self.minim_batt = 0.
        self.items = []
        
    def change_batt(self, dQ):
        chrg = self.battery + dQ
        if chrg >= self.battery_cap:
            self.battery = self.battery_cap
        elif chrg <= self.minim_batt:
            self.battery = self.minim_batt
        else:
            self.battery = chrg
        
  #Rovers move up, down, left, right
    def AgentMoves(self, board, x, y):
        moves = [ ]
        for dx, dy in [(-1,0),(1,0),(0,-1),(0,1)]:
            nx = x+dx
            ny = y+dy
            if board.isValidMove(nx, ny):
                moves.append((nx,ny))
        return moves
    
    def MakeAgentBitmap(self, board): # a grid holding a single agent's position
        bmp = np.zeros((board.height, board.width))
        bmp[self.pos[0]][self.pos[1]] = 1
        return bmp
    def ChangeBitPosition(self,board, to_pos, from_pos):
        """most logic encoded here -- agent interacts with environment by proximity&movement"""
        if self.bitmap is None:
            self.bitmap = self.MakeAgentBitmap(board)
        w_pos, h_pos = to_pos 
        frw, frh = from_pos
        
        if board.isValidMove(w_pos, h_pos) and self.isFunctioning():
            self.bitmap[frh][frw] = 0
            self.bitmap[h_pos][w_pos] = 1
            self.pos = to_pos
            # below changes agents battery, inventory, reward/score
            self.ChangeAgentState(board, "movement", to_pos = to_pos, from_pos = from_pos) # battery
            if board.goalpoints[self.pos[1]][self.pos[0]] == 1:
                self.items.append(1) # pick up item
                board.goalpoints[self.pos[1]][self.pos[0]] = 0 # item disappears from board
                c.vprint("ITEM PICKED UP \n \n \n")
                
            if board.image[self.pos[1]][self.pos[0]] < 0. and len(self.items) is not 0: # if in a negative zone (+energy)
                # boost score!
                # item dropped off / disappears
                board.score += len(self.items)
                self.items = []
                
        return
                
    def isFunctioning(self):
        # asseses whether agent is functioning
        if self.battery <= self.minim_batt + 1.0:
            return False
        return True
    
    def movement_cost(self,board, to_pos, from_pos):
        pos_x, pos_y = from_pos
        return -1.0*(board.image[pos_y][pos_x])
    
    def ChangeAgentState(self, board, move_type, to_pos = None, from_pos = None):
        # update state of agent as a result of actions
        if move_type == "movement":
            dQ = self.movement_cost(board,to_pos,from_pos)
            
        else: dQ = 0.
        self.change_batt(dQ)
        #if move_type == "drill":
            #dQ = self.wait_gain(board)
     
        
    def movestring_to_newcoord(self,mv):
        if mv == "u":
            u = self.pos[1] - 1       
            pos = ( self.pos[0], u)
        if mv == "d":
            d = self.pos[1] + 1
            pos = ( self.pos[0], d)
        if mv == "l":
            l = self.pos[0] - 1
            pos = ( l, self.pos[1])
        if mv == "r":
            r = self.pos[0] + 1
            pos = ( r, self.pos[1])    
        return pos
    
    def qm(self,board, mv):
        # quick command to move and print: agent.qm(brd, "r")   "u","d","l","r"
        new_pos = self.movestring_to_newcoord(mv)
        self.ChangeBitPosition(board, new_pos, self.pos)
        c.vprint(self.bitmap)
        c.vprint("shown above (x,y) : " + str(self.pos) + "    battery: " + str(self.battery))
          
"""
    def qm(self,board, mv):
        # quick command to move and print: agent.qm(brd, "r")   "u","d","l","r"
        if mv == "u":
            u = self.pos[1] - 1
            self.ChangeBitPosition(board,( self.pos[0], u), self.pos)
        if mv == "d":
            d = self.pos[1] + 1
            self.ChangeBitPosition(board,( self.pos[0], d), self.pos)
        if mv == "l":
            l = self.pos[0] - 1
            self.ChangeBitPosition(board,( l, self.pos[1]), self.pos)
        if mv == "r":
            r = self.pos[0] + 1
            self.ChangeBitPosition(board,( r, self.pos[1]), self.pos)
        c.vprint(self.bitmap)
        c.vprint("shown above (x,y) : " + str(self.pos) + "    battery: " + str(self.battery))
"""    
    
class Board():
    def __init__(self, agents, image = None, height = None, width = None, goalpoints = None, score = 0, time = None, max_time = None):
        "Set up initial board configuration."
        self.height = height or DEFAULT_HEIGHT
        self.width = width or DEFAULT_WIDTH

        self.image = image
        if self.image is not None:
            self.height = np.shape(self.image)[0]
            self.width = np.shape(self.image)[1]
        else: # make a random board increasingly difficult
            im = np.arange(self.height*self.width)
            im = im.reshape(self.height, self.width)
            self.image = (im-.1*np.median(im))*random.random()

        if max_time is None:
            self.max_time = 5
        else: self.max_time = max_time
        if time is None:
            self.time = 0 # integer 
        else: self.time = time
        
        self.score = 0 # to evaluate wins/losses at the end
        
        if goalpoints is None:
            # random * random * random goal-points
            self.goalpoints = np.random.randint(0,2,size=(self.height, self.width))    *    np.random.randint(0,2,size=(self.height, self.width))   *   np.random.randint(0,2,size=(self.height, self.width))
            
        else: self.goalpoints = goalpoints
        
        self.agents = agents
        self.n_agents = len(agents)
        
        self.board2 = None # manual initialization

        for a in agents:
            if a.pos is None: a.pos = [self.height, self.width] # all init_pos at the top left if pos == None
            if a.bitmap is None: a.bitmap = a.MakeAgentBitmap(self)
            
        self.state = self.aggregate_state() # terrain + locations; needs updating at each timestep
        
        self.actionsPerAgent = 4
        acts = [['u','d','l','r']]*self.n_agents 
        self.acts = [act for act in itertools.product(*acts)] # all possible joint actions  [(rover1act1, rover2act1, rover3act1),...,(roverNactM, ...)]
        
    def init_p2_board(self):
        self.board2 = copy.deepcopy(self)
        
    def aggregate_actions(self):
        return self.acts
    def aggregate_state(self):
        total_state = [a.bitmap for a in self.agents]
        total_state.append(self.image)
        total_state.append(self.goalpoints)
        return np.concatenate(total_state)

    def __getitem__(self, index): 
        return self.agents[index]
    
    def printgame(self):
        # agents represented as negative numbers
        tmp = self.image
        tmp = tmp.round(1)
        print("updating total map...")
        print("image size: " + str(np.shape(tmp)) + "   height/width: " + str((self.height, self.width)))
        for a in self.agents:
            if tmp[a.pos[1]][a.pos[0]] > 0.0 or tmp[a.pos[1]][a.pos[0]]%1 != 0:
                tmp[a.pos[1]][a.pos[0]] = 0.0
            tmp[a.pos[1]][a.pos[0]] -= 100
        print("printing total map... -n*100 means n agents occupy that square")
        print(tmp)
        print("\nGoalpoints");print(self.goalpoints)
        

    def isValidMove(self, x, y):
        c.vprint("Is valid move?  x: " + str(x) + "   y: " + str(y))
        if x < 0 or x >= self.width or y < 0 or y >= self.height:
            return False
        #if self.image[y][x] is '#obstacle#':
        #    return False
        return True
    
    def is_valid_move(self, action, color):
        # action for all agents: action = (a1, a2, ... , a_n_agents)
        if color == -1:
            b = self.board2
        if color == 1:
            b = self
            
        agts = b.agents
        for i in range(len(agts)):
            new_coord = agts[i].movestring_to_newcoord(action[i])
            if self.isValidMove(*new_coord) is False:
                return False
        return True
    
    def get_valid_moves(self):
        moves = [True for a in self.acts]  # just assume all are valid -- masked upon execution
        return moves
    
    def isgameDone(self, player):
        # this will pass to a function that compares players
        # player = + or - 1        
        if self.time == self.max_time and player == -1:
            return True
        total_items = sum([len(a.items) for a in self.agents])
        if np.max(self.goalpoints) == 0. and total_items == 0: # if all points collected and returned
            return True 
        return False


    def _move(self, move, color):
        """Perform the given move on the board
        move : e.g. for 3 agents   ("u","d","r")
        """
        if color == -1:
            agents2 = self.board2.agents
            for i in range(len(agents2)):
                agents2[i].qm(self.board2, move[i])
                
        if color == 1: 
            for i in range(len(self.agents)):
                self.agents[i].qm(self, move[i]) # moves up/down/left/right
        self.time +=1 #increment time after a join move is made
def main1():
    ags = [Agent() for i in range(10)]
    b = Board(ags)
    a0 = ags[0]
    b.printgame()
    print("Returned agnt, board as variables a, b \n -- use \n agnt.qm(board, \"u or d or l or r\") ")
    print("    board.printgame()\n    board.agents # list of agents \n    ")
    return a0, b

def all_random_step(b):
    
    for a in b.agents:
        mv = random.random()
        if mv <.25: a.qm(b, "r")
        if mv >= 25. and mv <.50: a.qm(b, "l")
        if mv >=.5 and mv<.75: a.qm(b, "u")
        if mv >=.75 and mv < 1.0: a.qm(b, "d")
    
    
def main2_random():
    n = 3
    timesteps = 60
    ags = [Agent() for i in range(n)]
    brd = Board(ags)
    for i in range(timesteps):
        mv_num = np.random.randint(0,len(brd.acts))
        brd._move(brd.acts[mv_num], 1)
        brd.printgame()
        print(" Timestep: " + str(brd.time) + "    Total Inventory: " + str(sum([len(a.items) for a in brd.agents])))
        print("Score: " + str(brd.score))
        if brd.isgameDone(-1):
            return ags, brd
    return ags, brd

#a, b = main1()
a, b = main2_random()

