from __future__ import print_function
import sys
sys.path.append('..')
from Game import Game
from MAPPERLogic import Board
from MAPPERLogic import Agent
import numpy as np


class MAPPERGame(Game):
    def __init__(self, n_agents=3, max_time = 50):
        self.max_time = max_time
        self.n_agents = n_agents
        
        self.board = None
        
    def getInitBoard(self):
        # return initial board (numpy board)
        #agents, image = None, height = None,
        #width = None, goalpoints = None, score = 0, time = None, max_time = None
        agents = [Agent() for i in range(self.n_agents)]
        self.board = Board(agents, maxt_time = self.max_time)
        self.board1.init_p2_board() #  player 2 / deepcopy player 1 board  board.board2
        
        return self.board.aggregate_state() # this is just the board for player 1

    def getBoardSize(self):
        # (height+n_agents)*width
        return np.shape(self.board.state)

    def getActionSize(self):
        # return number of actions: 
        #         -->naive/brute-force 4 actions per agent, jointly = 4^n
        return int(4**self.n_agents)

    def getNextState(self, board, player, action):
        # if player takes action on board, return next (board,player)
        # action must be a valid move
        board._move(action, player)
        
        return (b.aggregate_state(), -player)

    # modified
    def getValidMoves(self, board, player):
        return board.get_valid_moves()

    # modified
    def getGameEnded(self, board, player):
        if board.isgameDone(player):
            score1 = board.score
            score2 = board.board2.score
            if score1 > score2:
                return 1
            if score2 > score1:
                return -1
            if score1 == score2:
                return -1 # if they are still tied, give player2 the win
        else:
            return 0
        
    def getCanonicalForm(self, board, player):
            # return state if player==1, else return -state if player==-1
            # two separate boards with the same init conditions and evolution: board1, board2 
        if player == 1:
            return self.board
        if player == -1:
            return self.board.board2
    # modified
    def getSymmetries(self, board, pi):
        # mirror, rotational
        assert(len(pi) == self.n**2 + 1)  # 1 for pass
        pi_board = np.reshape(pi[:-1], (self.n, self.n))
        l = []

        for i in range(1, 5):
            for j in [True, False]:
                newB = np.rot90(board, i)
                newPi = np.rot90(pi_board, i)
                if j:
                    newB = np.fliplr(newB)
                    newPi = np.fliplr(newPi)
                l += [(newB, list(newPi.ravel()) + [pi[-1]])]
        return l

    def stringRepresentation(self, board):
        # 8x8 numpy array (canonical board)
        return board.state.tostring()


def display(board):
    n = board.shape[0]

    for y in range(n):
        print(y, "|", end="")
    print("")
    print(" -----------------------")
    for y in range(n):
        print(y, "|", end="")    # print the row #
        for x in range(n):
            piece = board[y][x]    # get the piece to print
            if piece == -1:
                print("b ", end="")
            elif piece == 1:
                print("W ", end="")
            else:
                if x == n:
                    print("-", end="")
                else:
                    print("- ", end="")
        print("|")

    print("   -----------------------")
