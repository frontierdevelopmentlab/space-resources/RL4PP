import numpy as np
import pylab as plt
import networkx as nx
import matplotlib.pyplot as plt
import math


'''
This code is extracted from 
https://amunategui.github.io/reinforcement-learning/index.html

'''
# 
# Generic constants
# 

point_list1 = []

#Total nondes
grid_side= 17
nodes = grid_side*grid_side
map_size = math.sqrt( nodes )
map_size_1 = map_size - 1

TRAINING_ITERATIONS=100#000
DEBUG=False

#Create grid
for f in range (nodes):
    if (0 != ((f + 1)%map_size) ) and (f+1 < nodes):
        value = (f,f+1)
        point_list1.append(value)

for i in range (nodes):
    if  ((i+int(map_size)) < nodes ) :
        value = (i,i+int(map_size))
        point_list1.append(value)
        #print("Value:", i, i+int(map_size))

#print point_list1

points_list = point_list1

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Visualization
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#G=nx.Graph()
#G.add_edges_from(point_list1)
#pos = nx.spring_layout(G)
#nx.draw_networkx_nodes(G,pos)
#nx.draw_networkx_edges(G,pos)
#nx.draw_networkx_labels(G,pos)
#plt.show()


goal = 15

# how many points in graph? x points
MATRIX_SIZE = nodes

# create matrix x*y
R = np.matrix(np.ones(shape=(MATRIX_SIZE, MATRIX_SIZE)))
R *= -1
R = R.astype("float32")

if(DEBUG):
    print( R)

# assign zeros to paths and 100 to goal-reaching point
for point in points_list:
    if point[1] == goal:
        R[point] = 100
    else:
        R[point] = 0

    if point[0] == goal:
        R[point[::-1]] = 100
    else:
        # reverse of point
        R[point[::-1]]= 0

# add goal point round trip
R[goal,goal]= 100

Q = np.matrix(np.zeros([MATRIX_SIZE,MATRIX_SIZE]))
Q = Q.astype("float32")

# learning parameter
gamma = 0.8

initial_state = 1

def available_actions(state):
    current_state_row = R[state,]
    av_act = np.where(current_state_row >= 0)[1]
    return av_act

available_act = available_actions(initial_state) 

def sample_next_action(available_actions_range):
    next_action = int(np.random.choice(available_act,1))
    return next_action

action = sample_next_action(available_act)

def update(current_state, action, gamma):
    
  max_index = np.where(Q[action,] == np.max(Q[action,]))[1]
  
  if max_index.shape[0] > 1:
      max_index = int(np.random.choice(max_index, size = 1))
  else:
      max_index = int(max_index)
  max_value = Q[action, max_index]
  
  Q[current_state, action] = R[current_state, action] + gamma * max_value
  if(DEBUG):
      print('max_value', R[current_state, action] + gamma * max_value)
  
  if (np.max(Q) > 0):
    return(np.sum(Q/np.max(Q)*100))
  else:
    return (0)
    
update(initial_state, action, gamma)


## Training
scores = []
for i in range(TRAINING_ITERATIONS):
    current_state = np.random.randint(0, int(Q.shape[0]))
    available_act = available_actions(current_state)
    action = sample_next_action(available_act)
    score = update(current_state,action,gamma)
    scores.append(score)
    print('.')
    if(DEBUG):
        print ('Score:', str(score))

if(DEBUG):
    print("Trained Q matrix:")
    print(Q/np.max(Q)*100)

# Testing
current_state = 0
steps = [current_state]

while current_state != goal:

    next_step_index = np.where(Q[current_state,] == np.max(Q[current_state,]))[1]
    
    if next_step_index.shape[0] > 1:
        next_step_index = int(np.random.choice(next_step_index, size = 1))
    else:
        next_step_index = int(next_step_index)
    
    steps.append(next_step_index)
    current_state = next_step_index

print("Most efficient path:")
print(steps)

#plt.plot(scores)
#plt.show()
